package io.axoniq.labs.chat.commandmodel;

import io.axoniq.labs.chat.coreapi.*;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.commandhandling.model.AggregateIdentifier;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.spring.stereotype.Aggregate;

import java.util.ArrayList;
import java.util.List;

import static org.axonframework.commandhandling.model.AggregateLifecycle.apply;

@Aggregate
public class ChatRoom {

    @AggregateIdentifier
    private String roomId;
    private String name;
    private List<String> participants = new ArrayList<>();

    public ChatRoom() {

    }

    @CommandHandler
    public ChatRoom(CreateRoomCommand cmd) {
        apply(new RoomCreatedEvent(cmd.getRoomId(), cmd.getName()));
    }

    @CommandHandler
    public void joinRoomCommand(JoinRoomCommand cmd) {
        if (!this.participants.contains(cmd.getParticipant())) {
            apply(new ParticipantJoinedRoomEvent(cmd.getParticipant(), cmd.getRoomId()));
        }
    }

    @CommandHandler
    public void leaveRoomCommand(LeaveRoomCommand cmd) {
        if (this.participants.contains(cmd.getParticipant())) {
            apply(new ParticipantLeftRoomEvent(cmd.getParticipant(), cmd.getRoomId()));
        }
    }

    @CommandHandler
    public void postMessageCommand(PostMessageCommand cmd) {
        if (!this.participants.contains(cmd.getParticipant())) {
            throw new IllegalStateException(cmd.getParticipant() + "is not a member of room: " + cmd.getRoomId());
        }

        apply(new MessagePostedEvent(cmd.getParticipant(), cmd.getRoomId(), cmd.getMessage()));
    }

    @EventSourcingHandler
    public void roomCreated(RoomCreatedEvent roomCreatedEvent) {
        this.roomId = roomCreatedEvent.getRoomId();
        this.name = roomCreatedEvent.getName();
    }

    @EventSourcingHandler
    public void participantJoinedRoomEvent(ParticipantJoinedRoomEvent event) {
        this.participants.add(event.getParticipant());
    }

    @EventSourcingHandler
    public void participantLeftRoomEvent(ParticipantLeftRoomEvent event) {
        this.participants.remove(event.getParticipant());
    }
}
